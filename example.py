import requests

HOST = "https://release-gs.qa-playground.com/api/v1"

# response = requests.post(
#     url=f"{HOST}/setup",
#     headers={"Authorization": "Bearer "}
# )

response = requests.get(
    url=f"{HOST}/users",
    headers={"Authorization": "Bearer "}
)

print(response.json())